<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function form()
    {
        return view('form');
    }

    public function awal()
    {
        return view('pages.table');
    }

    public function table()
    {
        return view('pages.table');
    }

    public function datatable()
    {
        return view('pages.data-table');
    }
}
