<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\HomeController;

Route::get('/', 'HomeController@awal');

Route::get('/register', 'HomeController@form');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/table', 'HomeController@table');

Route::get('/data-table', 'HomeController@datatable');
